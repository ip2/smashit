//
//  swift
//  SmashIT
//
//  Created by student on 10.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class Ball: SKSpriteNode {
    
    init(texture: SKTexture, radius: CGFloat) {
        super.init(texture: texture, color: UIColor.clear, size: CGSize(width: CalcSize.calcRelativeNodeWidth(width: ((radius * 2))), height: CalcSize.calcRelativeNodeWidth(width: ((radius * 2)))))
        zPosition = 1
    
        physicsBody = SKPhysicsBody(circleOfRadius: self.size.width / 2)
        physicsBody?.usesPreciseCollisionDetection = true

        //categories:
        physicsBody?.categoryBitMask = GameScene.ballCategory
        physicsBody?.collisionBitMask = GameScene.playerCategory | GameScene.wallCategory
        physicsBody?.contactTestBitMask =  GameScene.goalCategory
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func getPosition() -> CGPoint {
        return position
    }
    
    func resetPosition() {
        physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        physicsBody?.angularVelocity = 0
        run(SKAction.move(to: CGPoint(x: 0, y: 0), duration: 0))
    }
}
