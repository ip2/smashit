//
//  swift
//  SmashIT
//
//  Created by student on 10.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//


import SpriteKit

class Player: SKSpriteNode {
    
    init(texture: SKTexture, radius: Int) {
        super.init(texture: texture, color: UIColor.clear, size: CGSize(width: radius*2, height: radius*2))
        
        zPosition = 1
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func getPosition() -> CGPoint {
        return position
    }
   
    func highlightPlayer() {
        let colorize = SKAction.colorize(with: self.color, colorBlendFactor: 0, duration: 0.2)
        self.run(colorize)
    }
    
    func grayOutPlayer() {
        let colorize = SKAction.colorize(with: .black, colorBlendFactor: 0.6, duration: 0.2)
        self.run(colorize)
    }
}
