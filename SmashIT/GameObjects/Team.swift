//
//  Team.swift
//  SmashIT
//
//  Created by student on 30.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation
import SpriteKit

class Team {
    
    let name: String
    let texture: String
    var score: Int = 0
    var players: [Player] = []
    var isCurrentlyPlaying: Bool
    private var playerId: String?
    
    init(name: String, texture: String, isCurrentlyPlaying: Bool, amount: Int) {
        self.name = name
        self.texture = texture
        self.isCurrentlyPlaying = isCurrentlyPlaying
        initializeTeam(teamtexture: texture, amount: amount)
    }
    
    fileprivate func initializeTeam(teamtexture: String, amount: Int) {
        for _ in 0..<amount {
            let player = Player(texture: SKTexture(imageNamed: teamtexture), radius: Int(CalcSize.calcRelativeNodeWidth(width: 50)))
            players.append(player)
        }
    }
    
    func resetPositions(positions: [CGPoint]) {
        for i in 0..<players.count {
            players[i].physicsBody?.velocity = CGVector(dx: 0, dy: 0)
            players[i].run(SKAction.move(to: positions[i], duration: 0))
        }
    }
    
    func setCurrentlyPlaying() {
        isCurrentlyPlaying = true
        highlightPlayers()
    }
    
    func setCurrentlyWaiting() {
        isCurrentlyPlaying = false
        grayOutPlayers()
    }
    
    func toggleCurrentlyPlaying() {
        isCurrentlyPlaying = !isCurrentlyPlaying
        isCurrentlyPlaying ? highlightPlayers() : grayOutPlayers()
    }
    
    fileprivate func highlightPlayers() {
        for p in players {
            p.highlightPlayer()
        }
    }
    
    fileprivate func grayOutPlayers() {
        for p in players {
            p.grayOutPlayer()
        }
    }
    
    func getPlayerId() -> String {
        return self.playerId!
    }
    
    func setPlayerId(playerId: String) {
        self.playerId = playerId
    }
}
