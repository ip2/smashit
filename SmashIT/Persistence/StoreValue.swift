//
//  StoreValue.swift
//  SmashIT
//
//  Created by student on 15.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation

class StoreValue {
    
    func saveIconValue(iconValue: String, key: String) {
        let iconDefault = UserDefaults.standard
        iconDefault.set(iconValue, forKey: key)
        iconDefault.synchronize()
    }
    
    func retrieveIconValue(soundIconActive: Bool, key: String) -> String {
        if UserDefaults.standard.string(forKey: key) != nil {
            return UserDefaults.standard.string(forKey: key)!
        } else {
            if soundIconActive {
                return "sound"
            } else {
                return "mute"
            }
        }
    }
}
