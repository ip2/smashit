//
//  LaunchScreenScene.swift
//  SmashIT
//
//  Created by student on 16.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class LaunchScreenScene: SKScene {
    
    var startGame = SKLabelNode()
    var MainGuy = SKSpriteNode()
    var TextureAtlas = SKTextureAtlas()
    var TextureArray = [SKTexture]()
    
    override func didMove(to view: SKView) {
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
        setupElements()
        MainGuy.run(SKAction.repeatForever(SKAction.animate(with: TextureArray,timePerFrame:0.1)))
    }
    
    fileprivate func setupElements() {
        createBackground()
        createStartGameLabel(position: CalcSize.calcNodePosition(position: CGPoint(x: 0, y: 0.3)), btnText: "SmashIT",pfontsize: 200)
        createStartGameLabel(position: CalcSize.calcNodePosition(position: CGPoint(x: 0, y: 0)), btnText: "Tap to play",pfontsize: 70)
    }
    
    func animateButton(button: SKLabelNode){
        let green = SKAction.colorize(with: UIColor.green, colorBlendFactor: 1, duration: 0.7)
        let white = SKAction.colorize(with: UIColor.white, colorBlendFactor: 1, duration: 0.7)
        let scaleUp = SKAction.scale(to: 1.15, duration: 0.7)
        let scaleDown = SKAction.scale(to: 1.0, duration: 0.7)
        
        let sequence1 = SKAction.sequence([green, white])
        let sequence2 = SKAction.sequence([scaleUp, scaleDown])
        button.run(SKAction.repeatForever(sequence1))
        button.run(SKAction.repeatForever(sequence2))
    }
    
    fileprivate func createBackground(){
        
        TextureAtlas = SKTextureAtlas(named: "SplashScreenIMGS")
        
        for i in 1...TextureAtlas.textureNames.count-1{
            let name = "\(i).gif"
            TextureArray.append(SKTexture(imageNamed: name))
        }
        
        MainGuy = SKSpriteNode(imageNamed: TextureAtlas.textureNames[0])
        
        MainGuy.size = CGSize(width: frame.size.width*1.35, height: frame.size.height)
        MainGuy.alpha = 0.3
        MainGuy.position = CGPoint(x:0,y:0)
        self.addChild(MainGuy)
    }
    
    fileprivate func createStartGameLabel(position: CGPoint, btnText: String,pfontsize: Int) {
        startGame = SKLabelNode(text: btnText)
        startGame.position = position
        startGame.zPosition = 1
        startGame.fontSize = CalcSize.calcFontsize(fontSize: CGFloat(pfontsize))
        startGame.fontName = "AvenirNext-Bold"
        startGame.fontColor = .white
        if(btnText == "Tap to play"){
            animateButton(button: startGame)
        }
        
        addChild(startGame)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let startMenuScene = StartMenuScene(size: view!.bounds.size)
        startMenuScene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        view!.presentScene(startMenuScene)
    }
}
