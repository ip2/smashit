//
//  ExchangePlayerPosition.swift
//  SmashIT
//
//  Created by Adrian  Prieth on 14.06.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

//mport Foundation
import SpriteKit

class ExchangePlayerPosition: PowerUp {
   
    var position1: CGPoint!
    var position2: CGPoint!
    
    init(scene: GameScene) {
        let textureName = "exchangePlayer"
        let upTime: TimeInterval = 7
        super.init(key: "exchangePlayer", powerUpTextureName: textureName, scene: scene, upTime: upTime, affects: Affects.PICKED_UP_TEAM)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func doAction(pickedUpBy: [Player], otherPlayers: [Player]) {
        super.doAction(pickedUpBy: pickedUpBy, otherPlayers: otherPlayers)
        position1 = pickedUpBy.first?.position
        position2 = otherPlayers.first?.position
        
        otherPlayers.first?.removeFromParent()
        pickedUpBy.first?.removeFromParent()
        
        pickedUpBy.first?.position = position2
        otherPlayers.first?.position = position1
        
        gameScene.addChild(otherPlayers.first!)
        gameScene.addChild(pickedUpBy.first!)
        
        
    }
    
    override func revertAction() {
        super.revertAction()
        position1 = pickedUpBy?.first?.position
        position2 = otherPlayers?.first?.position
        
        otherPlayers?.first?.removeFromParent()
        pickedUpBy?.first?.removeFromParent()
        
        pickedUpBy?.first?.position = position2
        otherPlayers?.first?.position = position1
        
        gameScene.addChild(otherPlayers!.first!)
        gameScene.addChild(pickedUpBy!.first!)
    }
}
