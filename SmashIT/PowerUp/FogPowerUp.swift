//
//  FogPowerUp.swift
//  SmashIT
//
//  Created by Svenja on 11.06.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class FogPowerUp: PowerUp {
    var blurredView: UIImage!
    var blurredImageView : UIImageView!
    
    init(scene: GameScene) {
        let textureName = "fogPowerUp"
        let upTime: TimeInterval =  12
        
        super.init(key: "fogPowerUp", powerUpTextureName: textureName, scene: scene, upTime: upTime, affects: Affects.NONE)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func doAction(pickedUpBy: [Player], otherPlayers otherTeam: [Player]) {
        super.doAction(pickedUpBy: pickedUpBy, otherPlayers: otherTeam)
        blurredView = UIImage(named: "fog")
        blurredImageView = UIImageView(image: blurredView)
        blurredImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        blurredImageView.alpha = 0.8
        gameScene.view?.addSubview(blurredImageView)
    }
    
    override func revertAction() {
        super.revertAction()
        blurredImageView.removeFromSuperview()
    }
}
