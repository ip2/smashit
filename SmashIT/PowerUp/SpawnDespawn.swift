//
//  SpawnDespawn.swift
//  SmashIT
//
//  Created by Svenja on 22.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class SpawnDespawn {
    var scene: GameScene
    var spawnTimer: Timer!
    var powerUp: PowerUp!
    var powerUpsClasses: [PowerUp] = []
    var xPosition: CGFloat!
    var yPosition: CGFloat!
    
    init(scene: GameScene, active: Bool){
        self.scene = scene
        if active {
            initializeTimer(waitTime: 0)
        }
    }
    
    private func initializeTimer(waitTime: UInt32) {
        spawnTimer = Timer.scheduledTimer(timeInterval: grabRandomTime(waitTime: waitTime), target: self, selector: #selector(spawn), userInfo: nil, repeats: true)
    }
    
    @objc public func spawn() {
        // stop Timer and then start again so a new randomNumber is given
        spawnTimer.invalidate()
        
        // spawn a random power up
        let powerUpIndex = Int.random(in: 0..<powerUpsClasses.count)
        powerUp = powerUpsClasses[powerUpIndex]

        
        repeat {
            getRandomPosition()
        } while checkPositionCollision() == false
        
        let randomPosition = CGPoint(x: xPosition, y: yPosition)
        
        powerUp.setPosition(position: randomPosition)
        powerUp.isSpawned = true
        
        scene.spawnPowerUp(powerUp: powerUp)
    
        Timer.scheduledTimer(timeInterval: 8.0, target: self, selector: #selector(despawn), userInfo: nil, repeats: false)
        
        initializeTimer(waitTime: 10)
    }
    
    @objc public func despawn() {
        scene.despawnPowerUp(powerUp: powerUp)
        scene.unusedPowerUp.removeAll()
    }
    
    private func grabRandomTime(waitTime: UInt32) -> Double{
        // +1 so that it goes from 1 to 30
        let time = (Double) (arc4random_uniform(15) + 1 + waitTime)
        return time
    }
    
    private func adjust() {
        if (xPosition < 0) {
            xPosition += +30
        } else if (xPosition > 0) {
            xPosition -= 30
        }
        
        if (yPosition < 0) {
            yPosition += 30
        } else if (yPosition > 0) {
            yPosition -= 30
        }
    }
    
    public func addPowerUp(powerUp: PowerUp) {
        powerUpsClasses.append(powerUp)
    }
    
    public func getPowerUpByName(name: String?) -> PowerUp? {
        if (name == nil) {
            return nil
        }
        for p in powerUpsClasses {
            if p.getKey() == name {
                return p
            }
        }
        return nil
    }
    
    private func checkPositionCollision() -> Bool {
        for node in scene.team1.players {
            if (node.contains(CGPoint(x: xPosition, y: yPosition))) {
                return false
            }
        }
        for node in scene.team2.players {
            if (node.contains(CGPoint(x: xPosition, y: yPosition))) {
                return false
            }
        }
        return true
    }
    
    private func getRandomPosition() {
        // get random position
        let screenSize = UIScreen.main.bounds
        
        xPosition = (CGFloat) (arc4random_uniform((UInt32)(screenSize.width))) - screenSize.width / 2
        yPosition = (CGFloat) (arc4random_uniform((UInt32)(screenSize.height))) - screenSize.height / 2
        
        // no border spawning
        adjust()
    }
    
    public func onGameOver() {
        if (powerUp != nil && powerUp.isActive) {
            powerUp.revertAction()
        }
    }
}
