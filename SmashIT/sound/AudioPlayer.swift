//
//  AudioPlayer.swift
//  SmashIT
//
//  Created by Erik Wolf on 27.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import AVFoundation

class AudioPlayer{
    var issound : Bool
    var audioPlayer : AVAudioPlayer
    init(url: URL, sound: Bool) {
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
        } catch let error {
            print("Couldn't load \(url.lastPathComponent): \(error)")
            audioPlayer = AVAudioPlayer()
        }
        issound = sound
    }

    
}
