//
//  SoccerGoal.swift
//  SmashIT
//
//  Created by student on 24.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class IceHockeyGoal : Goal {
    
    override init(position: CGPoint, goalImage: String, goalLine: String, goalName: String) {
        super.init(position: position, goalImage: goalImage, goalLine: goalLine, goalName: goalName)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
