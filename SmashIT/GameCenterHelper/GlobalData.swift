//
//  GlobalData.swift
//  SmashIT
//
//  Created by student on 15.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation
import SpriteKit
import GameKit

class GlobalData: Codable {
    
    public enum DataType: Int, Codable {
        case NO_DATA
        case IMPULSE
        case POSITION
        case CHATMESSAGE
        case TEAMSELECT
        case GAMECONFIG
        case SENDMODE
        case POWERUP
    }
    
    private var team1 = [CGPoint](repeatElement(CGPoint(x: 0, y: 0), count: 5))
    private var team2 = [CGPoint](repeatElement(CGPoint(x: 0, y: 0), count: 5))
    private var ball: CGPoint!
    private var type: DataType?
    private var impulse: CGVector?
    private var playerPosition: CGPoint?
    private var chatMessage: String?
    private var teamSelect:String?
    private var gameConfig:GameConfig?
    private var sendMode:Bool?
    
    
    init(){
        self.type = DataType.NO_DATA
    }
    
    func getType() -> DataType {
        return self.type!
    }
    
    func setType(type: DataType) {
        self.type = type
    }
    
    func setImpulse(impulse: CGVector, playerPosition: CGPoint){
        self.impulse = impulse
        self.playerPosition = playerPosition
        self.type = DataType.IMPULSE
    }
    
    func getImpulse() -> (CGVector, CGPoint) {
        return (self.impulse!, self.playerPosition!)
    }
    
    func setPositions(team1: Team, team2: Team, ball: Ball, width: CGFloat, height: CGFloat) {

            for i in 0..<team1.players.count {
                self.team1[i].x = team1.players[i].getPosition().x / width
                self.team1[i].y = team1.players[i].getPosition().y / height
                self.team2[i].x = team2.players[i].getPosition().x / width
                self.team2[i].y = team2.players[i].getPosition().y / height
            }
            self.ball = ball.getPosition()
            self.ball.x = self.ball.x / width
            self.ball.y = self.ball.y / height
            //self.ball.y = ball.getPosition().y
            self.type = DataType.POSITION
    }
    
    func getPositions() -> ([CGPoint],[CGPoint],CGPoint) {
        return (team1, team2, ball)
    }
    
    func setMessage(chatMessage: String) {
        self.chatMessage = chatMessage
        self.type = DataType.CHATMESSAGE
    }
    
    func getMessage()-> String{
        return self.chatMessage!
    }
    
    func setTeam(teamSelect: String) {
        self.teamSelect = teamSelect
        self.type = DataType.TEAMSELECT
    }
    
    func getTeam() -> String {
        return self.teamSelect!
    }
    
    func setGameConfig(gameConfig: GameConfig) {
        self.gameConfig = gameConfig
        self.type = DataType.GAMECONFIG
    }
    
    func getGameConfig() -> GameConfig {
        return self.gameConfig!
    }
    
    func setSendMode(sendMode: Bool) {
        self.sendMode = sendMode
        self.type = DataType.SENDMODE
    }
    
    func getSendMode() -> Bool {
        return self.sendMode!
    }
    
}
