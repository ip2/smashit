//
//  PowerupData.swift
//  SmashIT
//
//  Created by Kai Widmaier on 02.06.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation
import SpriteKit
import GameKit

class PowerUpData: Codable {
    
    public var type: String?
    var position: CGPoint?
    var team1: Bool = true
    var isSpawned: Bool = false
    var isActivated: Bool = false
    var isDespawned: Bool = false
    
    init() {
        self.type = nil
        self.position = nil
    }
    
    init(type: String?, position: CGPoint?) {
        self.type = type
        self.position = position
    }
    
    func getPowerUp(scene: GameScene) -> PowerUp? {
        let powerUp = scene.spawnDespawn.getPowerUpByName(name: type)
        powerUp?.position = GameCenterHelper.helper.powerUpData.position!
        return powerUp
    }
}
