//
//  GameMode.swift
//  SmashIT
//
//  Created by student on 23.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

protocol GameMode {
    func getField(frame: CGRect) -> GameField
}
