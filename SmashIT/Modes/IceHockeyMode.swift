//
//  IceHockeyMode.swift
//  SmashIT
//
//  Created by student on 23.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import UIKit
import SpriteKit

class IceHockeyMode: GameMode {
    
    func getField(frame: CGRect) -> GameField {
        return IceHockeyField(texture: SKTexture(imageNamed: "icehockey_field"), frame: frame)
    }
}
