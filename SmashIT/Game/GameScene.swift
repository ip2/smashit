//
//  GameScene.swift
//  SmashIT
//
//  Created by student on 08.04.19.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    let achievementSpecial = GameCenterAchievement()
    static let noCategory:UInt32 = 0
    static let playerCategory:UInt32 = 0b1
    static let ballCategory:UInt32 = 0b1 << 1
    static let goalCategory:UInt32 = 0b1 << 2
    static let wallCategory:UInt32 = 0b1 << 3
    static let itemCategory:UInt32 = 0b1 << 4
    static let powerUpCategory: UInt32 = 0b1 << 5
    
    var startingPoint: CGPoint!
    var touchedPlayer: Player!
    
    let worldNode = SKNode()
    var gamePaused = false
    var pauseButton = SKSpriteNode()
    var surrenderDialog = SurrenderDialog()
    var delayTimer = GameStartDelayTimer()
    var goallinepos:CGFloat = 0.0
    var score: Score!
    var gameConfig: GameConfig
    var gameTimer: GameTimer!
    
    let worldSpeed: CGFloat = 1.0
    var turnBar: CountdownBar!
    let turnTime = 5
    
    var field: GameField!
    var ball: Ball!
    var team1: Team!
    var team2: Team!
    var team1Id:String?
    var team2Id:String?
    
    var spawnDespawn: SpawnDespawn!
    var unusedPowerUp: [PowerUp]! = []
    
    init(size: CGSize, gameConfig: GameConfig) {
        self.gameConfig = gameConfig
        super.init(size: size)
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        setupPauseButton()
        addChild(worldNode)
        chooseGameField()

        setUpWorld()
        setupGameMode()
        initializeTurnBar()
        initializeTeams()
        initializeScore()
        setupPhysicBodies()
        initSpawnDespawn()
        addPowerUps()
        setupDelayTimer()
        loadBackgroundMusic()
    }
    
    func initSpawnDespawn(){
        spawnDespawn = SpawnDespawn(scene: self, active: true)
    }
    
    func setupDelayTimer() {
        delayTimer.setUp(worldNode: worldNode, gameScene: self)
    }
    
    func addPowerUps() {
        if(spawnDespawn != nil){
            spawnDespawn.addPowerUp(powerUp: SnailOnOtherTeam(scene: self))
            spawnDespawn.addPowerUp(powerUp: SnailOnMyTeam(scene: self))
            spawnDespawn.addPowerUp(powerUp: ResizePowerUp(scene: self))
            spawnDespawn.addPowerUp(powerUp: ZeitstrafePowerUp(scene: self))
            spawnDespawn.addPowerUp(powerUp: PhantomBallPowerUp(scene: self))
            spawnDespawn.addPowerUp(powerUp: FogPowerUp(scene: self))
            spawnDespawn.addPowerUp(powerUp: ObstaclePowerUp(scene: self))
            spawnDespawn.addPowerUp(powerUp: ExchangePlayerPosition(scene:self))
        }
    }
    
    func initializeTurnBar() {
        turnBar = CountdownBar(width: Int(CalcSize.calcRelativeNodeWidth(width: 150)), height: Int(CalcSize.calcRelativeNodeHeight(height: 25)), time: turnTime, position: CalcSize.calcNodePosition(position: CGPoint(x: 0.5, y: 0.91)))
        worldNode.addChild(turnBar)
        turnBar.startTimer(funcToCall: nextRound)
    }
    
    fileprivate func initializeTeams() {
        var amount:Int
        switch gameConfig.gameField {
            case GameConfig.GameField.SOCCER:
                amount = 3
            case GameConfig.GameField.ICE_HOCKEY:
                amount = 5
        }
        team1 = Team(name: gameConfig.team1, texture: gameConfig.team1, isCurrentlyPlaying: true, amount:amount)
        team2 = Team(name: gameConfig.team2, texture: gameConfig.team2, isCurrentlyPlaying: false, amount:amount)
        addTeamToScene(team: team1)
        addTeamToScene(team: team2)
        resetPositions()
        toggleCurrentlyPlayingTeam()
    }
    
    fileprivate func initializeScore() {
        score = Score(team1: team1, team2: team2)
        worldNode.addChild(score)
    }
    
    fileprivate func addTeamToScene(team: Team) {
        for p in team.players {
            worldNode.addChild(p)
        }
    }
    
    func chooseGameField() {
        switch gameConfig.gameField {
            case GameConfig.GameField.SOCCER:
                createBall(name: "soccer_ball", radius: 30)
                createField(gameField: SoccerField(frame: frame))
            case GameConfig.GameField.ICE_HOCKEY:
                createBall(name: "icehockey_ball", radius: 20)
                createField(gameField: IceHockeyField(frame: frame))
        }
    }
    
    func setupGameMode() {
        if gameConfig.gameMode == GameConfig.GameMode.TIME {
            gameTimer = GameTimer()
            worldNode.addChild(gameTimer)
            gameTimer.remainingTime = gameConfig.gameEndCondition
            gameTimer.initializeGameTimer()
            gameTimer.startGameTimer(funcToCall: handleTimerExpiration)
        }
    }
    
    func setupPauseButton() {
        pauseButton = SKSpriteNode(imageNamed: "pauseButton")
        pauseButton.size = CalcSize.calcRelativeNodeSizeForSqare(size: 60)
        pauseButton.position = CalcSize.calcNodePosition(position: CGPoint(x: 0.93, y: 0.91))
        pauseButton.zPosition = 10
        addChild(pauseButton)
    }
    
    func loadBackgroundMusic() {
        if let url = Bundle.main.url(forResource: "play_sound_1_1", withExtension: "mp3") {
            let backgroundMusic = AVAudioPlayerPool.player(url: url, sound: false)!
            backgroundMusic.audioPlayer.numberOfLoops = -1
            backgroundMusic.audioPlayer.play()
        }
    }
    
    func pauseGame() {
        worldNode.isPaused = true
        physicsWorld.speed = 0
        surrenderDialog.createSurrenderDialog(scene: self, width: frame.width, height: frame.height)
    }
    
    fileprivate func setUpWorld() {
        physicsBody?.friction = 0
        physicsBody?.restitution = 1
        physicsBody?.isDynamic = false
        physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0) // no gravity
        physicsWorld.contactDelegate = self // contact handling
        physicsWorld.speed = worldSpeed
    }
    
    fileprivate func createBall(name: String, radius: CGFloat) {
        ball = Ball(texture: SKTexture(imageNamed: name), radius: radius)
        worldNode.addChild(ball)
    }
    
    fileprivate func createField(gameField: GameField) {
        field = gameField
        worldNode.addChild(field)
        let goals = field.getGoal()
        for goal in goals {
            goallinepos = goal.getPos()
            worldNode.addChild(goal)
        }
        let overlays = field.getOverlays()
        for overlay in overlays {
            worldNode.addChild(overlay)
        }
    }
    
    func setupPhysicBodies() {
        field.setPhysicBodyForPlayer(players: team1.players)
        field.setPhysicBodyForPlayer(players: team2.players)
        field.setPhysicsBodyForBall(ball: ball)
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        if(contact.bodyA.categoryBitMask == GameScene.powerUpCategory) {
            let powerUp = (contact.bodyA.node as! PowerUp)
            if let player = contact.bodyB.node as! Player? {
                if unusedPowerUp.contains(powerUp) == false {
                    invokePowerUpAction(powerUp: powerUp, pickedUpBy: getTeamOfPlayer(player: player), otherTeam: getOtherTeam(player: player))
                    unusedPowerUp.append(powerUp)
                }
            }
        }
        else if(contact.bodyB.categoryBitMask == GameScene.powerUpCategory) {
            let powerUp = (contact.bodyB.node as! PowerUp)
            if let player = contact.bodyA.node as! Player? {
                if unusedPowerUp.contains(powerUp) == false {
                    invokePowerUpAction(powerUp: powerUp, pickedUpBy: getTeamOfPlayer(player: player), otherTeam: getOtherTeam(player: player))
                    unusedPowerUp.append(powerUp)
                }
            }
        }
    }
    
    func invokePowerUpAction(powerUp: PowerUp, pickedUpBy: [Player], otherTeam: [Player]) {
        powerUp.doAction(pickedUpBy: pickedUpBy, otherPlayers: otherTeam)
    }
    
    func getTeamOfPlayer(player: Player) -> [Player] {
        return team1.players.contains(player) ? team1.players : team2.players
    }
    
    func getOtherTeam(player: Player) -> [Player] {
        return team1.players.contains(player) ? team2.players : team1.players
    }
    
    func didEnd(_ contact: SKPhysicsContact) {
        let categoryA = contact.bodyA.categoryBitMask
        let categoryB = contact.bodyB.categoryBitMask
        
        if categoryA == GameScene.goalCategory || categoryB == GameScene.goalCategory {
            let goalBody: SKNode = (categoryA == GameScene.goalCategory) ? contact.bodyA.node! : contact.bodyB.node!
            let ballNode: SKSpriteNode = (categoryA == GameScene.ballCategory) ? contact.bodyA.node! as! SKSpriteNode : contact.bodyB.node! as! SKSpriteNode
            let collidedCategory = (categoryA == GameScene.goalCategory) ? categoryB : categoryA
            if collidedCategory == GameScene.ballCategory{
                if goalBody.name == "leftGoal" && ballNode.position.x < goallinepos {
                    
                    goalScored(by: team1, against: team2)
                } else if goalBody.name == "rightGoal" && ballNode.position.x > -goallinepos {
                    goalScored(by: team2, against: team1)
                }
            }
        }
    }
    
    func goalScored(by: Team, against: Team) {
        by.score += 1
        if let url = Bundle.main.url(forResource: "torjubel_1_0", withExtension: "mp3") {
            let jubel = AVAudioPlayerPool.player(url: url, sound: true)!
            jubel.audioPlayer.play()
        }
        if (gameConfig.gameMode == GameConfig.GameMode.POINTS && checkMatchEndCondition(scoring: by)) ||
            (gameConfig.gameMode == GameConfig.GameMode.TIME) {
            score.showScore(nodeToPause: worldNode)
            by.setCurrentlyWaiting()
            against.setCurrentlyPlaying()
            resetPositions()
        }
    }
    
    func checkMatchEndCondition(scoring: Team) -> Bool {
        if scoring.score < gameConfig.gameEndCondition {
            return true
        } else {
            gameOver(winner: scoring)
            
            return false
        }
    }
    
    fileprivate func handleTimerExpiration() {
        if team1.score > team2.score {
            gameOver(winner: team1)
        } else if team1.score == team2.score {
            gameOver(winner: nil)
        } else {
            gameOver(winner: team2)
        }
    }
    
    func gameOver(winner: Team?){
        spawnDespawn.onGameOver()
        if(winner != nil){
            score.showFinalScoreDialog(nodeToPause: worldNode, winner: winner!.name)
        }
        else{
            score.showFinalScoreDialog(nodeToPause: worldNode, winner: "both")
        }
        resetPositions()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
            self.switchToStartMenu()
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: UITouch in touches {
            let location = touch.location(in: self)
            var tmp : [SKNode]
            startingPoint = (touches.first?.location(in: self))!
            tmp = nodes(at: location)
            for node in tmp {
                if node is Player {
                    touchedPlayer = node as? Player
                }
            }
        }
        if AppDelegate.wasMinimized && gamePaused {
            worldNode.isPaused = true
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let endingPoint = (touches.first?.location(in: self))!
        if (touchedPlayer != nil && getCurrentlyPlaying().contains(touchedPlayer) && !worldNode.isPaused) {
            let force = CGVector(dx: ( (endingPoint.x - startingPoint.x) * 2 ), dy: (endingPoint.y - startingPoint.y ) * 2)
            movePlayer(impulse: force)
            nextRound()
            touchedPlayer = nil
        } else if pauseButton.contains(endingPoint) && !worldNode.isPaused {
            AVAudioPlayerPool.playButtonClickSound()
            gamePaused = true
            pauseButton.removeFromParent()
            pauseGame()
        }
        if surrenderDialog.cancelButton.contains(endingPoint) && worldNode.isPaused {
            AVAudioPlayerPool.playButtonClickSound()
            gamePaused = false
            AppDelegate.wasMinimized = false
            surrenderDialog.removeDialog()
            setupPauseButton()
            worldNode.isPaused = false
            physicsWorld.speed = worldSpeed
        }
        if surrenderDialog.surrenderButton.contains(endingPoint) && worldNode.isPaused {
            AVAudioPlayerPool.playButtonClickSound()
            AppDelegate.wasMinimized = false
            switchToStartMenu()
        }
    }
    
    func spawnPowerUp(powerUp: PowerUp) {
        worldNode.addChild(powerUp)
    }
    
    func despawnPowerUp(powerUp: PowerUp) {
        if (powerUp.isSpawned) {
            powerUp.isSpawned = false
            powerUp.removeFromParent()
        }
    }
    
    func movePlayer(impulse: CGVector) {
        touchedPlayer.physicsBody?.applyImpulse(impulse)
    }
    
    func isPlayerInTeam1(player: Player) -> Bool {
        return team1.players.contains(player)
    }
    
    func getCurrentlyPlaying() -> [Player] {
        return team1.isCurrentlyPlaying ? team1.players : team2.players
    }
    
    func getCurrentlyPlayingPlayerId() -> String {
        return team1.isCurrentlyPlaying ? team1.getPlayerId() : team2.getPlayerId()
    }
    
    func nextRound() {
        turnBar.startTimer(funcToCall: nextRound)
        toggleCurrentlyPlayingTeam()
    }
    
    func toggleCurrentlyPlayingTeam() {
        team1.toggleCurrentlyPlaying()
        team2.toggleCurrentlyPlaying()
    }
    
    fileprivate func resetPositions() {
        team1.resetPositions(positions: field.getInitialPositionsTeam1())
        team2.resetPositions(positions: field.getInitialPositionsTeam2())
        ball.resetPosition()
        
        turnBar.startTimer(funcToCall: nextRound)
    }
    
    func switchToStartMenu() {
        AVAudioPlayerPool.stop()
        let startMenuScene = StartMenuScene(size: view!.bounds.size)
        startMenuScene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        view!.presentScene(startMenuScene)
        spawnDespawn.onGameOver()
        if spawnDespawn.spawnTimer != nil {
            spawnDespawn.spawnTimer.invalidate()
        }
    }
}
