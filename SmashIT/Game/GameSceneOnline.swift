//
//  GameSceneOnline.swift
//  SmashIT
//
//  Created by student on 15.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation
import SpriteKit
import GameKit

class GameSceneOnline: GameScene{
    
    let localPlayer = GameCenterHelper.helper.localPlayer
    let remotePlayer = GameCenterHelper.helper.remotePlayer
    var sendMode = false
    var globalData = GlobalData()

    let achievement = GameCenterAchievement()

    var width:CGFloat?
    var height:CGFloat?

    
    init(size: CGSize, gameConfig: GameConfig, sendMode: Bool) {
        super.init(size: size, gameConfig: gameConfig)
        width = size.width
        height = size.height
        self.sendMode = sendMode
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func didMove(to view: SKView) {
        super.didMove(to: view)
        initTeamIds()
    }
    
    override func initSpawnDespawn(){
        spawnDespawn = SpawnDespawn(scene: self, active: sendMode)
    }
    
    override func initializeTurnBar() {
        turnBar = CountdownBar(width: 150, height: 20, time: turnTime, position: CGPoint(x: self.frame.width / 2 - 250, y: self.frame.height / 2 - 35))
        worldNode.addChild(turnBar)
        if(sendMode){
            turnBar.startTimer(funcToCall: nextRound)
        }
        else{
            turnBar.startTimer(funcToCall: {})
        }
    }
    
    func initTeamIds() {
        if(sendMode) {
            team1Id = localPlayer?.playerID
            team2Id = remotePlayer?.playerID
            team1.setPlayerId(playerId: team1Id!)
            team2.setPlayerId(playerId: team2Id!)
        }
        else {
            team1Id = remotePlayer?.playerID
            team2Id = localPlayer?.playerID
            team1.setPlayerId(playerId: team1Id!)
            team2.setPlayerId(playerId: team2Id!)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let endingPoint = (touches.first?.location(in: self))!
        
        if worldNode.isPaused {
            if surrenderDialog.cancelButton.contains(endingPoint) {
                surrenderDialog.removeDialog()
                setupPauseButton()
                worldNode.isPaused = false
                physicsWorld.speed = worldSpeed
            }
            if surrenderDialog.surrenderButton.contains(endingPoint) {
                switchToStartMenu()
                let data = StateChange.playerLeft.rawValue.data(using: String.Encoding.utf8)
                do{
                    try GameCenterHelper.helper.currentMatch?.sendData(toAllPlayers: data!, with: GKMatch.SendDataMode.reliable)
                }
                catch{
                    print("Error when sending player left \(error)")
                }
            }
        } else {
            if touchedPlayer != nil && getCurrentlyPlaying().contains(touchedPlayer) && getCurrentlyPlayingPlayerId() == localPlayer?.playerID {
                let impulse = CGVector(dx: ( (endingPoint.x - startingPoint.x) * 2 ), dy: (endingPoint.y - startingPoint.y ) * 2)
                movePlayer(impulse: impulse)
            } else if pauseButton.contains(endingPoint) {
                pauseButton.removeFromParent()
                pauseGame()
            }
        }
    }
    
    override func didEnd(_ contact: SKPhysicsContact) {
        let categoryA = contact.bodyA.categoryBitMask
        let categoryB = contact.bodyB.categoryBitMask
        
        if categoryA == GameScene.goalCategory || categoryB == GameScene.goalCategory {
            let goalBody: SKNode = (categoryA == GameScene.goalCategory) ? contact.bodyA.node! : contact.bodyB.node!
            let ballNode: SKSpriteNode = (categoryA == GameScene.ballCategory) ? contact.bodyA.node! as! SKSpriteNode : contact.bodyB.node! as! SKSpriteNode
            let collidedCategory = (categoryA == GameScene.goalCategory) ? categoryB : categoryA
            if collidedCategory == GameScene.ballCategory && sendMode{
                if goalBody.name == "leftGoal" && ballNode.position.x < goallinepos {
                    let data = StateChange.goalTeam1.rawValue.data(using: String.Encoding.utf8)
                    do{
                        try GameCenterHelper.helper.currentMatch?.sendData(toAllPlayers: data!, with: GKMatch.SendDataMode.reliable)
                        
                    }
                    catch{
                        print("Error when sending goal by team 1 \(error)")
                    }
                    goalScored(by: team1, against: team2)
                    if(team1Id == localPlayer?.playerID){
                        achievement.setGoalAchievements()
                    }
                    
                } else if goalBody.name == "rightGoal" && ballNode.position.x > -goallinepos {
                    let data = StateChange.goalTeam2.rawValue.data(using: String.Encoding.utf8)
                    do{
                        try GameCenterHelper.helper.currentMatch?.sendData(toAllPlayers: data!, with: GKMatch.SendDataMode.reliable)
                        
                    }
                    catch{
                        print("Error when sending goal by team 2 \(error)")
                    }
                    goalScored(by: team2, against: team1)
                    if(team2Id == localPlayer?.playerID){
                      achievement.setGoalAchievements()
                    }
                }
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        if(sendMode) {
            if GameCenterHelper.helper.globalData.getType() == GlobalData.DataType.IMPULSE {
                GameCenterHelper.helper.globalData.setType(type: GlobalData.DataType.NO_DATA)
                let tmpData = GameCenterHelper.helper.globalData.getImpulse()
                let tmpPlayers = nodes(at: CGPoint(x: tmpData.1.x * width!, y: tmpData.1.y * height!))
                for player in tmpPlayers {
                    if player is Player {
                        let impulse = CGVector(dx: tmpData.0.dx , dy: tmpData.0.dy )
                        player.physicsBody?.applyImpulse(impulse)
                        nextRound()
                    }
                }
            } else {
                globalData.setPositions(team1: team1, team2: team2, ball: ball, width: width!, height: height!)
                let encoder = JSONEncoder()
                if let encoded = try? encoder.encode(globalData) {
                    do{
                        try
                            GameCenterHelper.helper.currentMatch?.sendData(toAllPlayers: encoded, with: GKMatch.SendDataMode.unreliable)
                    }
                    catch{
                        print("Error when sending Position Data")
                    }
                }
            }
        } else if(GameCenterHelper.helper.globalData.getType() == GlobalData.DataType.POSITION) {
            GameCenterHelper.helper.globalData.setType(type: GlobalData.DataType.NO_DATA)
            let tmpData = GameCenterHelper.helper.globalData.getPositions()
            for i in 0..<team1.players.count {
                moveToPos(object: team1.players[i], pos: CGPoint(x: tmpData.0[i].x * frame.size.width, y: tmpData.0[i].y * frame.size.height))
                moveToPos(object: team2.players[i], pos: CGPoint(x: tmpData.1[i].x * frame.size.width, y: tmpData.1[i].y * frame.size.height))
            }
            moveToPos(object: ball, pos: CGPoint(x: tmpData.2.x * frame.size.width, y: tmpData.2.y * frame.size.height))
        }
        if(!sendMode){
            if(GameCenterHelper.helper.nextRound){
                print("Nextround initiated")
                nextRound()
                GameCenterHelper.helper.nextRound = false
            }
            if(GameCenterHelper.helper.goalTeam1){
                goalScored(by: team1, against: team2)
                if(team1Id == localPlayer?.playerID){
                  achievement.setGoalAchievements()
                    
                }
                GameCenterHelper.helper.goalTeam1 = false
            }
            if(GameCenterHelper.helper.goalTeam2){
                goalScored(by: team2, against: team1)
                if(team2Id == localPlayer?.playerID) {
               achievement.setGoalAchievements()
                }
                GameCenterHelper.helper.goalTeam2 = false
            }
            if(GameCenterHelper.helper.powerUpData.isActivated) {
                print("Power up invoked received")
                GameCenterHelper.helper.powerUpData.isActivated = false
                let pickedUpBy = GameCenterHelper.helper.powerUpData.team1 ? team1 : team2
                let otherTeam = GameCenterHelper.helper.powerUpData.team1 ? team2 : team1
                GameCenterHelper.helper.powerUpData.getPowerUp(scene: self)?
                    .doAction(pickedUpBy: pickedUpBy!.players, otherPlayers: otherTeam!.players)
            }
            else if(GameCenterHelper.helper.powerUpData.isSpawned) {
                print("Power up spawned received")
                GameCenterHelper.helper.powerUpData.isSpawned = false
                let tmpPowerUp = GameCenterHelper.helper.powerUpData.getPowerUp(scene: self)!
                tmpPowerUp.isSpawned = true
                tmpPowerUp.position = CGPoint(x: tmpPowerUp.position.x * width!, y: tmpPowerUp.position.y * height!)
                spawnPowerUp(powerUp: tmpPowerUp)
            }
            else if(GameCenterHelper.helper.powerUpData.isDespawned) {
                print("Power up despawned received")
                GameCenterHelper.helper.powerUpData.isDespawned = false
                despawnPowerUp(powerUp: GameCenterHelper.helper.powerUpData.getPowerUp(scene: self)!)
            }
        }
        if(GameCenterHelper.helper.playerLeft){
            let currentView = view
            showPlayerLeftMessage()
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                self.displayStartMenu(currentView: currentView!)
                GameCenterHelper.helper.currentMatch?.disconnect()
                GameCenterHelper.helper.currentMatch?.delegate = nil
                GameCenterHelper.helper.currentMatch = nil
                GameCenterHelper.helper.playerLeft = false
            })
        }
    }
    
    fileprivate func showPlayerLeftMessage() {
        let background = SKSpriteNode(color: UIColor.black, size: CGSize(width: frame.size.width,
                                                                             height: frame.size.height))
        background.alpha = 0.7
        background.zPosition = 99
        
        let message = SKLabelNode(text: "Your opponent surrendered!")
        message.fontColor = UIColor.white
        message.fontName = "AvenirNext-Bold"
        message.fontSize = CGFloat(integerLiteral: 40)
        background.addChild(message)
        addChild(background)
    }
    
    func moveToPos(object: SKSpriteNode, pos: CGPoint) {
        object.run(SKAction.move(to: pos, duration: 0.03))
    }
    
    override func gameOver(winner: Team?) {
       achievement.setGamesPlayed()
        if(gameConfig.gameMode == GameConfig.GameMode.TIME && gameConfig.gameEndCondition == 5400){
            //90 Minutes match was played
            achievement.setPlay90MinutesAchievment()
        }
        print("Winner ID: " + winner!.getPlayerId() + " | playerID: " + localPlayer!.playerID)
        if(winner!.getPlayerId() == localPlayer?.playerID){
            achievement.setWinAchievements()
          //  print("Achievement set For Winning Game")
        }
        super.gameOver(winner: winner)
    }
    
    override func movePlayer(impulse: CGVector) {
        if(sendMode){
            touchedPlayer.physicsBody?.applyImpulse(CGVector(dx: impulse.dx, dy: impulse.dy))
            nextRound()
        }
        else {
            globalData.setImpulse(impulse: impulse, playerPosition: CGPoint(x: touchedPlayer.position.x / width!, y: touchedPlayer.position.y / height!))
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(globalData) {
                do{
                    try GameCenterHelper.helper.currentMatch?.sendData(toAllPlayers: encoded, with: GKMatch.SendDataMode.reliable)
                }
                catch{
                    print("Error when sending Force Vector")
                }
            }
        }
        touchedPlayer = nil
    }
    
    override func invokePowerUpAction(powerUp: PowerUp, pickedUpBy: [Player], otherTeam: [Player]) {
        if (sendMode) {
            super.invokePowerUpAction(powerUp: powerUp, pickedUpBy: pickedUpBy, otherTeam: otherTeam)
            sendPowerUpInvoked(powerUp: powerUp, team1: isPlayerInTeam1(player: pickedUpBy[0]))
        }
    }
    
    override func spawnPowerUp(powerUp: PowerUp) {
        powerUp.removeFromParent()
        worldNode.addChild(powerUp)
        if(sendMode) {
            sendPowerUpSpawned(powerUp: powerUp)
        }
    }
    
    override func despawnPowerUp(powerUp: PowerUp) {
        if (powerUp.isSpawned) {
            powerUp.isSpawned = false
            powerUp.removeFromParent()
        }
        if(sendMode) {
            sendPowerUpDespawned(powerUp: powerUp)
        }
    }
    
    func sendPowerUpSpawned(powerUp: PowerUp) {
        let data = PowerUpData(type: powerUp.getKey(), position: CGPoint(x: powerUp.position.x / width!, y: powerUp.position.y / height!))
        data.isSpawned = true
        data.isActivated = false
        data.isDespawned = false
        sendPowerUpData(data: data)
    }
    
    func sendPowerUpDespawned(powerUp: PowerUp) {
        let data = PowerUpData(type: powerUp.getKey(), position: powerUp.position)
        data.isSpawned = false
        data.isActivated = false
        data.isDespawned = true
        sendPowerUpData(data: data)
    }
    
    func sendPowerUpInvoked(powerUp: PowerUp, team1: Bool) {
        let data = PowerUpData(type: powerUp.getKey(), position: powerUp.position)
        data.isSpawned = false
        data.isActivated = true
        data.isDespawned = false
        data.team1 = team1
        sendPowerUpData(data: data)
    }
    
    func sendPowerUpData(data: PowerUpData) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(data) {
            do{
                try
                    GameCenterHelper.helper.currentMatch?.sendData(toAllPlayers: encoded, with: GKMatch.SendDataMode.reliable)
            }
            catch {
                print("Error when sending Force Vector")
            }
        }
    }
    
    override func nextRound() {
        toggleCurrentlyPlayingTeam()
        if(sendMode){
            print("Next round")
            turnBar.startTimer(funcToCall: nextRound)
            let data = StateChange.nextRound.rawValue.data(using: String.Encoding.utf8)
            do{
                print("Next round sent")
                try GameCenterHelper.helper.currentMatch?.sendData(toAllPlayers: data!, with: GKMatch.SendDataMode.reliable)
                
            }
            catch{
                print("Error when sending next round \(error)")
            }
        }
        else{
            turnBar.startTimer(funcToCall: {})
        }
    }
    
    func displayStartMenu(currentView: SKView) {
        AVAudioPlayerPool.stop()
        let startMenuScene = StartMenuScene(size: currentView.bounds.size)
        startMenuScene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        currentView.presentScene(startMenuScene)
        if sendMode {
            spawnDespawn.spawnTimer.invalidate()
        }
    }
}
