//
//  GameConfig.swift
//  SmashIT
//
//  Created by student on 26.04.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

class GameConfig: Codable{
    
    public enum GameField: Int, Codable {
        case SOCCER
        case ICE_HOCKEY
    }
    
    public enum GameMode: Int, Codable {
        case POINTS
        case TIME
    }
    
    private var _gameField: GameField
    private var _gameMode: GameMode
    private var _team1: String
    private var _team2: String
    private var _gameEndCondition: Int
    
    init() {
        _gameField = GameField.SOCCER
        _gameMode = GameMode.TIME
        _team1 = "germany"
        _team2 = "switzerland"
        _gameEndCondition = 60
    }
    
    var gameField: GameField {
        set { _gameField = newValue }
        get { return _gameField }
    }
    
    var gameMode: GameMode {
        set {
            _gameMode = newValue
        }
        get { return _gameMode }
    }
    
    var team1: String {
        set { _team1 = newValue }
        get { return _team1 }
    }
    
    var team2: String {
        set { _team2 = newValue }
        get { return _team2 }
    }
    
    var gameEndCondition: Int {
        set {
            _gameEndCondition = newValue }
        get { return _gameEndCondition }
    }
}
