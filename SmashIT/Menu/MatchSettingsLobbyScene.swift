//
//  MatchSettingsScene.swift
//  SmashIT
//
//  Created by student on 03.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation
import SpriteKit
import GameKit

class MatchSettingsLobbyScene: MatchSettingsScene {
    
    let localPlayer = GameCenterHelper.helper.localPlayer
    let remotePlayer = GameCenterHelper.helper.remotePlayer
    let globalData = GlobalData()
    var sendMode = false
    var gameConfig = GameConfig()
    
    var waitLbl = SKLabelNode()
    
    var btnSendMessage = SKLabelNode()
    var headerSender = SKLabelNode()
    var inputField: UITextField!
    var gameTableView = GameRoomTableView()
    
    override func didMove(to view: SKView) {
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
        sendMode = localPlayer!.playerID < remotePlayer!.playerID
        setupElements()
    }
    
    
    func sendRandomBool() {
        sendMode = Bool.random()
        globalData.setSendMode(sendMode: sendMode)
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(globalData) {
            do{
                try
                    GameCenterHelper.helper.currentMatch?.sendData(toAllPlayers: encoded, with: GKMatch.SendDataMode.reliable)
            }
            catch{
                print("Error when sending Force Vector")
            }
            
        }
    }
    
    func recieveBool() {
        if(GameCenterHelper.helper.globalData.getType() == GlobalData.DataType.SENDMODE) {
            GameCenterHelper.helper.globalData.setType(type: GlobalData.DataType.NO_DATA)
            self.sendMode = !GameCenterHelper.helper.globalData.getSendMode()
        }
        
    }
    
    override func setupElements() {
        AVAudioPlayerPool.stop()
        loadBackgroundMusik()
        createTable()
        createBackButton()
        createSendMessageButton() // --> Bugfix indem Send-Button zuerst erstellt wird und dann InputLabel
        createInputLabel()
        setupSeparators()
        if sendMode {
            createWaitLabel()
            fieldSelector = createFieldSelector()
            team1Selector = createTeamSelector(
                size: CGSize(width: size.width / 2 - 20, height: 3 * size.height / 10),
                position: CGPoint(x: -(size.width / 4 + 10), y: 0), title: "Select team 1")
            modeSelector = createModeSelector()
            modeSettingsSelector = createModeSettingsSelector()
        } else {
            createStartGameButton()
            team2Selector = createTeamSelector(
                size: CGSize(width: size.width / 2 - 20, height: 3 * size.height / 10),
                position: CGPoint(x: -(size.width / 4 + 10), y: 0), title: "Select team 2")
        }
    }
    
    override func setupSeparators() {
        let fieldBottomBar = SKSpriteNode(color: UIColor.gray, size: CGSize(width: frame.size.width/2, height: 3))
        fieldBottomBar.position = CGPoint(x: -frame.size.width/4, y: frame.size.height / 2 - frame.size.height / 3)
        addChild(fieldBottomBar)
        let teamBottomBar = SKSpriteNode(color: UIColor.gray, size: CGSize(width: frame.size.width, height: 3))
        teamBottomBar.position = CGPoint(x: 0, y: frame.size.height / 2 - 2 * frame.size.height / 3)
        addChild(teamBottomBar)
        let splitScreenVertical = SKSpriteNode(color: UIColor.gray, size: CGSize(width: 3, height: frame.size.height))
        splitScreenVertical.position = CGPoint(x: 0, y: 0)
        addChild(splitScreenVertical)
    }
    
    fileprivate func createWaitLabel() {
        waitLbl = SKLabelNode(text: "Wait for other player")
        waitLbl.fontSize = CalcSize.calcFontsize(fontSize: 45)
        waitLbl.fontColor = .green
        waitLbl.alpha = 0.5
        waitLbl.position = CGPoint(x: frame.size.width / 4, y: 4 * frame.size.height / 10)
        addChild(waitLbl)
    }
    
    override func createStartGameButton() {
        btnPlayGame.text = "Start Game"
        btnPlayGame.fontSize = CalcSize.calcFontsize(fontSize: 50)
        btnPlayGame.position = CGPoint(x: frame.size.width / 4, y: 4 * frame.size.height / 10)
        addChild(btnPlayGame)
        if !sendMode {
            animateButton(button: btnPlayGame, wichanimation: 1)
        }
    }
    
    func animateButton(button: SKLabelNode, wichanimation: Int){
        let green = SKAction.colorize(with: UIColor.green, colorBlendFactor: 1, duration: 0.7)
        let white = SKAction.colorize(with: UIColor.white, colorBlendFactor: 1, duration: 0.7)
        let scaleUp = SKAction.scale(to: 1.15, duration: 0.7)
        let scaleDown = SKAction.scale(to: 1.0, duration: 0.7)
        
        let sequence1 = SKAction.sequence([green, white])
        let sequence2 = SKAction.sequence([scaleUp, scaleDown])
        
        if (wichanimation == 1) {
            button.run(SKAction.repeatForever(sequence1))
            button.run(SKAction.repeatForever(sequence2))
        } else if (wichanimation == 2) {
            button.run(SKAction.repeat(sequence1, count: 1))
            button.run(SKAction.repeat(sequence2, count: 1))
        }
    }
    
    fileprivate func createTable() {
        gameTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        gameTableView.frame=CGRect(x: (frame.size.width / 20 * 11), y: (size.height/20)*3, width: frame.size.width / 20 * 8, height: frame.size.width / 20 * 6)
        view!.addSubview(gameTableView)
        gameTableView.reloadData()
    }
    
    fileprivate func createInputLabel() {
        inputField = UITextField(frame: CGRect(x: (frame.size.width / 20 * 11), y: (size.height/20 * 11.7), width: frame.size.width / 20 * 6, height: 40))
        view!.addSubview(inputField)
        inputField.layer.cornerRadius = 8
        inputField.textColor = SKColor.black
        inputField.placeholder = "  Deine Nachricht..."
        inputField.backgroundColor = SKColor.white
        inputField.autocorrectionType = UITextAutocorrectionType.yes
        inputField.clearButtonMode = UITextField.ViewMode.whileEditing
        inputField.backgroundColor = UIColor.lightGray
        inputField.textColor = UIColor.white
        view!.addSubview(inputField)
    }
    
    fileprivate func createSendMessageButton() {
        btnSendMessage.text = "senden"
        btnSendMessage.fontSize = CalcSize.calcFontsize(fontSize: 40)
        btnSendMessage.position = CGPoint(x: (size.width / 20 * 8), y: -(size.height / 20 * 2.5))
        addChild(btnSendMessage)
    }
    
    override func createFieldSelector() -> ItemSelector {
        var fields: [SelectableImageItem] = []
        fields.append(SelectableImageItem(value: GameConfig.GameField.SOCCER, text: "Soccer", image: "soccer_field_selection"))
        fields.append(SelectableImageItem(value: GameConfig.GameField.ICE_HOCKEY, text: "Ice Hockey", image: "icehockey_field_selection"))
        let selectorSize = CGSize(width: size.width / 2 - 20, height: 3 * size.height / 10)
        let selector = ItemSelector(size: selectorSize, itemDistance: 0.5, items: fields, title: "Select a field", horizontal: true)
        selector.position = CGPoint(x: -(size.width / 4 + 10), y: size.height / 3)
        addChild(selector)
        return selector
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let location = (touches.first?.location(in: self))!
        if self.children.contains(btnPlayGame) && btnPlayGame.contains(location) {
            if sendMode {
                
                if(team1Selector.currentItem.value as! String == GameCenterHelper.helper.globalData.getTeam()){
                    inputField.backgroundColor = UIColor.clear
                    gameTableView.backgroundColor = UIColor.clear
                    inputField.isUserInteractionEnabled = false
                    inputField.textColor = UIColor.white
                    sameTeamDialog.createSameTeamDialog(scene: self, width: frame.width, height: frame.height)
                    
                }else{
                    inputField.removeFromSuperview()
                    gameTableView.removeFromSuperview()
                    AVAudioPlayerPool.stop()
                    AVAudioPlayerPool.playButtonClickSound()

                    
                    globalData.setGameConfig(gameConfig: getOnlineGameConfig())
                    let encoder = JSONEncoder()
                    if let encoded = try? encoder.encode(globalData) {
                        do{
                            try
                                GameCenterHelper.helper.currentMatch?.sendData(toAllPlayers: encoded, with: GKMatch.SendDataMode.reliable)
                            print("sent gameconfig", gameConfig)
                        }
                        catch{
                            print("Error when sending Force Vector")
                        }
                    }
                    let gameSceneOnline = GameSceneOnline(size: view!.bounds.size, gameConfig: getOnlineGameConfig(), sendMode: sendMode)
                    inputField.removeFromSuperview()
                    gameTableView.removeFromSuperview()
                    self.view!.presentScene(gameSceneOnline)
                }
            } else {
                AVAudioPlayerPool.playButtonClickSound()
                let teamSelect = team2Selector.currentItem.value as! String
                globalData.setTeam(teamSelect: teamSelect)
                let encoder = JSONEncoder()
                if let encoded = try? encoder.encode(globalData) {
                    do{
                        
                        try
                            GameCenterHelper.helper.currentMatch?.sendData(toAllPlayers: encoded, with: GKMatch.SendDataMode.reliable)
                    }
                    catch{
                        print("Error when sending Force Vector")
                    }
                    
                }
                btnPlayGame.removeAllActions()
                btnPlayGame.removeFromParent()
                createWaitLabel()
            }
        }
        if btnBack.contains(location) {
            AVAudioPlayerPool.stop()
            AVAudioPlayerPool.playButtonClickSound()
            inputField.removeFromSuperview()
            gameTableView.removeFromSuperview()
            GameCenterHelper.helper.currentMatch?.disconnect()
            GameCenterHelper.helper.currentMatch?.delegate = nil
            GameCenterHelper.helper.currentMatch = nil
            GameCenterHelper.helper.playerLeft = false
            let startMenu = StartMenuScene(size: view!.bounds.size)
            startMenu.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            view!.presentScene(startMenu)
        }
        if btnSendMessage.contains(location){
            animateButton(button: btnSendMessage,wichanimation: 2)
            AVAudioPlayerPool.playButtonClickSound()
            
            //Wenn textfeld leer ist, dann nichts in Table schreiben
            if(inputField.text! != ""){
                
                gameTableView.addRow(item: inputField.text!)
                globalData.setMessage(chatMessage: inputField.text!)
                let encoder = JSONEncoder()
                if let encoded = try? encoder.encode(globalData) {
                    do {
                        try GameCenterHelper.helper.currentMatch?.sendData(toAllPlayers: encoded, with: GKMatch.SendDataMode.reliable)
                    } catch {
                        print("Error when sending Force Vector")
                    }
                }
                gameTableView.reloadData()
                inputField.text = ""
            }
        }
        if sameTeamDialog.agreeButton.contains(location){
            inputField.backgroundColor = UIColor.lightGray
            gameTableView.backgroundColor = UIColor.lightGray
            inputField.isUserInteractionEnabled = true
            inputField.textColor = UIColor.white
            sameTeamDialog.removeDialog()
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        if(sendMode){
            if(GameCenterHelper.helper.globalData.getType() == GlobalData.DataType.TEAMSELECT) {
                waitLbl.removeFromParent()
                createStartGameButton()
                GameCenterHelper.helper.globalData.setType(type: GlobalData.DataType.NO_DATA)
                let tmpData = GameCenterHelper.helper.globalData.getTeam()
                gameConfig.team2 = tmpData
                animateButton(button: btnPlayGame,wichanimation: 1)
            }
        } else {
            if(GameCenterHelper.helper.globalData.getType() == GlobalData.DataType.GAMECONFIG) {
                GameCenterHelper.helper.globalData.setType(type: GlobalData.DataType.NO_DATA)
                let tmpData = GameCenterHelper.helper.globalData.getGameConfig()
                let gameSceneOnline = GameSceneOnline(size: view!.bounds.size, gameConfig: tmpData, sendMode: sendMode)
                AVAudioPlayerPool.stop()
                inputField.removeFromSuperview()
                gameTableView.removeFromSuperview()
                self.view!.presentScene(gameSceneOnline)
            }
        }
        
        if(GameCenterHelper.helper.globalData.getType() == GlobalData.DataType.CHATMESSAGE){
            GameCenterHelper.helper.globalData.setType(type: GlobalData.DataType.NO_DATA)
            let tmpData = GameCenterHelper.helper.globalData.getMessage()
            gameTableView.addRow(item: tmpData)
            gameTableView.reloadData()
            print(tmpData)
        }
    }
    
    func getOnlineGameConfig() -> GameConfig {
        gameConfig.gameField = fieldSelector.currentItem.value as! GameConfig.GameField
        gameConfig.team1 = team1Selector.currentItem.value as! String
        gameConfig.gameMode = modeSelector.currentItem.value as! GameConfig.GameMode
        gameConfig.gameEndCondition = modeSettingsSelector.currentItem.value as! Int
        return gameConfig
    }
    
}
    class GameRoomTableView: UITableView,UITableViewDelegate,UITableViewDataSource {
        
        var globalData = GlobalData()
        var items: [String] = [""]
        
        override init(frame: CGRect, style: UITableView.Style) {
            super.init(frame: frame, style: style)
            self.delegate = self
            self.dataSource = self
            self.layer.cornerRadius = 8
            self.backgroundColor = UIColor.lightGray
            self.separatorInset = UIEdgeInsets.zero
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        // MARK: - Table view data source
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return items.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
            cell.textLabel?.text = items[indexPath.row]
            cell.backgroundColor = UIColor.lightGray
            cell.separatorInset = UIEdgeInsets.zero
            cell.textLabel?.textColor = UIColor.white
            // Hier könnte man eine Animation einfügen
            //cell.alpha = 0
            //UIView.animate(withDuration: 0.5, animations: { cell.alpha = 1 })
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            return "  Chat"
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            print("You selected cell #\(indexPath.row)!")
        }
        
        func addRow( item: String){
            let formatter = DateFormatter()
            // initially set the format based on your datepicker date / server String
            formatter.dateFormat = "HH:mm"
            let myString = formatter.string(from: Date())
            
            items.insert(item + " " + myString ,at: 0)
        }
        
        func loadBackgroundMusik(){
            if let url = Bundle.main.url(forResource: "settingmusic_1_0", withExtension: "mp3") {
                let backgroundMusik = AVAudioPlayerPool.player(url: url, sound: false)!
                backgroundMusik.audioPlayer.numberOfLoops = -1
                backgroundMusik.audioPlayer.play()
            }
        }
}
