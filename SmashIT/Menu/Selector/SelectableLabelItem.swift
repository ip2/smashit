//
//  SelectableLabelItem.swift
//  SmashIT
//
//  Created by student on 09.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation
import SpriteKit

class SelectableLabelItem: ItemSelector.Item {
    
    var itemLabel = SKLabelNode()
    var itemText: String
    
    init(value: Int, text: String) {
        self.itemText = text
        super.init(value: value)
        createLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createLabel() {
        itemLabel = SKLabelNode(text: itemText)
        itemLabel.fontSize = CalcSize.calcFontsize(fontSize: 45)
        itemLabel.position = CGPoint(x: 0, y: 0)
        addChild(itemLabel)
    }
    
    override func highlightItem() {
        itemLabel.run(SKAction.scale(to: 1.3, duration: 0.5))
    }
    
    override func normalizeItem() {
        itemLabel.run(SKAction.scale(to: 1.0, duration: 0.5))
    }
    
    override func getItemSize(width: CGFloat) -> CGSize {
        return CGSize(width: itemLabel.frame.size.width, height: itemLabel.frame.size.height)
    }
}
