//
//  ItemSelector.swift
//  SmashIT
//
//  Created by student on 03.05.19.
//  inspired by https://github.com/aornano/Spritekit-HorizontalScroll
//

import Foundation
import SpriteKit

class ItemSelector: SKCropNode {
    
    var itemDistance: CGFloat
    var lastPosition: CGFloat = 0.0
    var touchableArea: CGRect
    var moveableArea = SKSpriteNode()
    var titleLabel = SKLabelNode()
    var size: CGSize
    
    var items = [Item]()
    var currentItemIndex: Int = 0
    var lastItem: Item
    var currentItem: Item
    var itemPosition = [CGPoint]()
    
    var horizontal: Bool = true
    
    var onItemChanged: ((Item) -> ())?
    var touchedOnOutsideListener: (() -> ())?
    var onSliderNotMoved: (() -> ())?
    
    init(size: CGSize, itemDistance: CGFloat, items: [Item], title: String, horizontal: Bool) {
        self.size = size
        self.touchableArea = CGRect(origin: CGPoint(x: -size.width / 2, y: -size.height / 2), size: size)
        self.items = items
        self.itemDistance = itemDistance
        self.lastItem = items[0]
        self.currentItem = items[0]
        self.horizontal = horizontal
        super.init()
        isUserInteractionEnabled = true
        maskNode = SKSpriteNode(color: SKColor.gray, size: size)
        
        createTitle(title)
        createMovableArea()
        addItems()
        currentItem.highlightItem()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func replaceAllItems(items: [Item]) {
        self.moveableArea.removeAllChildren()
        self.items.removeAll()
        self.items = items
        addItems()
    }
    
    fileprivate func createTitle(_ title: String) {
        titleLabel = SKLabelNode(text: title)
        titleLabel.fontSize = CalcSize.calcFontsize(fontSize: 53)
        titleLabel.zPosition = 1
        titleLabel.position = CGPoint(x: 0, y: size.height / 2 - titleLabel.frame.size.height)
        addChild(titleLabel)
    }
    
    fileprivate func createMovableArea() {
        let cropNode = SKCropNode()
        cropNode.maskNode = SKSpriteNode(color: SKColor.gray, size: CGSize(width: size.width, height: size.height - titleLabel.frame.height))
        cropNode.position = CGPoint(x: 0, y: -titleLabel.frame.height)
        moveableArea.position = CGPoint(x: 0, y: 0)
        moveableArea.name = "moveable area"
        cropNode.addChild(moveableArea)
        addChild(cropNode)
    }
    
    private func addItems() {
        for i in 0..<items.count {
            let item = items[i]
            item.setItemSize(height: size.height - (0.37 * size.height))
            item.position = horizontal
                ? CGPoint(x: CGFloat(i) * calcItemDistance(), y: 0)
                : CGPoint(x: 0, y: CGFloat(i) * calcItemDistance())
            moveableArea.addChild(item)
        }
        moveableArea.size = horizontal
            ? CGSize(width: calcItemDistance(), height: size.height - 2 * titleLabel.frame.size.height)
            : CGSize(width: items[0].getItemSize(width: size.width).width, height: calcItemDistance())
    }
    
    private func calcItemDistance() -> CGFloat {
        if items.count > 0 {
            let itemSize = items[0].getItemSize(width: size.width)
            return (itemDistance + 1) * (horizontal ? itemSize.width : itemSize.height)
        }
        return size.width / 2
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self)
        if let touchedNode = atPoint(location) as? SKSpriteNode {
            if touchedNode != self {
                lastPosition = horizontal ? location.x : location.y
                tapNode(node: touchedNode)
            }
        }
        if !self.maskNode!.contains(location) {
            touchedOnOutsideListener?.self()
        }
    }
    
    fileprivate func tapNode(node: SKNode) {
        // hier kann man auf klicks reagieren
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self)
        
        if touchableArea.contains(location) {
            let currentPosition = horizontal ? location.x : location.y
            let startLimit: CGFloat = CGFloat(items.count - 1)
            let endLimit: CGFloat = 1.0
            let scrollSpeed: CGFloat = 1.0
            let movAreaPos = horizontal ? moveableArea.position.x : moveableArea.position.y
            let newPostion = movAreaPos + ((currentPosition - lastPosition) * scrollSpeed)
            if newPostion < (horizontal ? moveableArea.size.width : moveableArea.size.height) * -startLimit {
                moveableArea.position = horizontal
                    ? CGPoint(x: moveableArea.size.width * -startLimit, y: moveableArea.position.y)
                    : CGPoint(x: moveableArea.position.x, y: moveableArea.size.height * -startLimit)
            } else if newPostion > (horizontal ? moveableArea.size.width : moveableArea.size.height) * endLimit {
                moveableArea.position = horizontal
                    ? CGPoint(x: moveableArea.size.width * endLimit, y: moveableArea.position.y)
                    : CGPoint(x: moveableArea.position.x, y: moveableArea.size.height * endLimit)
            } else {
                moveableArea.position = horizontal
                    ? CGPoint(x: newPostion, y: moveableArea.position.y)
                    : CGPoint(x: moveableArea.position.x, y: newPostion)
            }
            
            // detect current visible item
            itemPosition = [CGPoint]()
            for i in 0..<items.count {
                let startLimit = horizontal
                    ? moveableArea.size.width / 2 - (moveableArea.size.width * CGFloat(i))
                    : moveableArea.size.height / 2 - (moveableArea.size.height * CGFloat(i))
                let endLimit = horizontal
                    ? moveableArea.size.width / 2 - (moveableArea.size.width * CGFloat(i + 1))
                    : moveableArea.size.height / 2 - (moveableArea.size.height * CGFloat(i + 1))
                if endLimit ... startLimit ~= (horizontal ? moveableArea.position.x : moveableArea.position.y) {
                    currentItemIndex = i
                    currentItem = items[i]
                    currentItem.highlightItem()
                } else {
                    items[i].normalizeItem()
                }
                itemPosition.append(horizontal
                    ? CGPoint(x: (endLimit + (startLimit - endLimit) / 2), y: moveableArea.position.y)
                    : CGPoint(x: moveableArea.position.x, y: (endLimit + (startLimit - endLimit) / 2)))
            }
            lastPosition = currentPosition
        }
    }
    
    public func setOnItemChangeListener(callback: @escaping (Item) -> ()) {
        self.onItemChanged = callback
    }
    
    public func setTouchedOnOutsiteListener(callback: @escaping () -> ()) {
        self.touchedOnOutsideListener = callback
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if itemPosition.count > 0 && moveableArea.action(forKey: "moveAction") == nil {
            let moveAction = SKAction.move(to: itemPosition[currentItemIndex], duration: 0.5)
            moveAction.timingMode = .easeInEaseOut
            moveableArea.run(moveAction, withKey: "moveAction")
            if lastItem != currentItem {
                onItemChanged?.self(currentItem)
                lastItem = currentItem
            }
        }
    }
    
    class Item: SKNode {
        
        var value: Any
        
        init(value: Any) {
            self.value = value
            super.init()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func setItemSize(height: CGFloat) {
        }
        
        func highlightItem() {
        }
        
        func normalizeItem() {
        }
        
        func getItemSize(width: CGFloat) -> CGSize {
            return CGSize(width: 0, height: 0)
        }
    }
}
