//
//  Helper.swift
//  SmashIT
//
//  Created by student on 15.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class CreateNodeHelper {
    
    func createSpriteNode(imageName: String, size: CGSize, position: CGPoint, zPosition: CGFloat, physicsBody: Bool, bodyRadius: CGFloat) -> SKSpriteNode {
        let node = SKSpriteNode(imageNamed: imageName)
        node.size = size
        node.position = position
        node.zPosition = zPosition
        
        if physicsBody {
            node.physicsBody = SKPhysicsBody(circleOfRadius: bodyRadius)
        }
        return node
    }
    
    func createLabelNode(text: String, position: CGPoint, zPosition: CGFloat, fontSize: CGFloat, color: UIColor) -> SKLabelNode {
        let label = SKLabelNode(text: text)
        label.position = position
        label.zPosition = zPosition
        label.fontSize = fontSize
        label.fontName = "AvenirNext-Bold"
        label.fontColor = .white
        return label
    }
}
