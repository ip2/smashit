//
//  MatchSettingsScene.swift
//  SmashIT
//
//  Created by student on 03.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation
import SpriteKit

class MatchSettingsScene: SKScene {
    var btnPlayGame = SKLabelNode()
    var btnBack = SKLabelNode()
    
    static let teamNames = ["brazil", "cameroon", "canada", "cuba", "ecuador", "egypt", "france", "germany", "greece", "iceland", "india", "italy", "japan", "mexico", "netherlands", "newzealand", "norway", "philippines", "portugal", "russia", "southafrica", "southkorea", "spain", "sweden", "switzerland", "usa"]
    var fieldSelector: ItemSelector!
    var team1Selector: ItemSelector!
    var team2Selector: ItemSelector!
    var modeSelector: ItemSelector!
    var modeSettingsSelector: ItemSelector!
    var modeTimeItems: [ItemSelector.Item] = []
    var modePointsItems: [ItemSelector.Item] = []
    var sameTeamDialog = SameTeamDialog()
    
    override func didMove(to view: SKView) {
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
        setupElements()
    }
    
    func setupElements() {
        loadBackgroundMusik()
        createStartGameButton()
        createBackButton()
        fieldSelector = createFieldSelector()
        team1Selector = createTeamSelector(
            size: CGSize(width: size.width / 2 - 20, height: 3 * size.height / 10),
            position: CGPoint(x: size.width / 4 + 10, y: 0), title: "Select team 2")
        team1Selector.setTouchedOnOutsiteListener {
            self.team1Selector.zPosition = 10
            self.team2Selector.zPosition = 20
        }
        
        team2Selector = createTeamSelector(
            size: CGSize(width: size.width / 2 - 20, height: 3 * size.height / 10),
            position: CGPoint(x: -(size.width / 4 + 10), y: 0), title: "Select team 1")
        team2Selector.setTouchedOnOutsiteListener {
            self.team1Selector.zPosition = 20
            self.team2Selector.zPosition = 10
        }
        
        modeSelector = createModeSelector()
        modeSettingsSelector = createModeSettingsSelector()
        setupSeparators()
    }
    
    func setupSeparators() {
        let fieldBottomBar = SKSpriteNode(color: UIColor.gray, size: CGSize(width: frame.size.width, height: 3))
        fieldBottomBar.position = CGPoint(x: 0, y: frame.size.height / 2 - frame.size.height / 3)
        addChild(fieldBottomBar)
        let teamBottomBar = SKSpriteNode(color: UIColor.gray, size: CGSize(width: frame.size.width, height: 3))
        teamBottomBar.position = CGPoint(x: 0, y: frame.size.height / 2 - 2 * frame.size.height / 3)
        addChild(teamBottomBar)
        let verticalBar = SKSpriteNode(color: UIColor.gray, size: CGSize(width: 3, height: frame.size.height * 2 / 3))
        verticalBar.position = CGPoint(x: 0, y: frame.size.height / 2 - 2 * frame.size.height / 3)
        addChild(verticalBar)
    }
    
    func createBackButton() {
        btnBack.text = "Back"
        btnBack.fontSize = CalcSize.calcFontsize(fontSize: 40)
        btnBack.position = CalcSize.calcNodePosition(position: CGPoint(x: -0.9, y: 0.9))
        addChild(btnBack)
    }
    
    func createStartGameButton() {
        btnPlayGame.text = "Start Game"
        btnPlayGame.fontSize = CalcSize.calcFontsize(fontSize: 50)
        btnPlayGame.position = CGPoint(x: 2 * frame.size.width / 6, y: 3 * frame.size.height / 10)
        addChild(btnPlayGame)
        animateButton(button: btnPlayGame)
    }
    
    func animateButton(button: SKLabelNode){
        let green = SKAction.colorize(with: UIColor.green, colorBlendFactor: 1, duration: 0.7)
        let white = SKAction.colorize(with: UIColor.white, colorBlendFactor: 1, duration: 0.7)
        let scaleUp = SKAction.scale(to: 1.15, duration: 0.7)
        let scaleDown = SKAction.scale(to: 1.0, duration: 0.7)
        
        let sequence1 = SKAction.sequence([green, white])
        let sequence2 = SKAction.sequence([scaleUp, scaleDown])
        button.run(SKAction.repeatForever(sequence1))
        button.run(SKAction.repeatForever(sequence2))
    }
    
    func createFieldSelector() -> ItemSelector {
        var fields: [SelectableImageItem] = []
        fields.append(SelectableImageItem(value: GameConfig.GameField.SOCCER, text: "Soccer", image: "soccer_field_selection"))
        fields.append(SelectableImageItem(value: GameConfig.GameField.ICE_HOCKEY, text: "Ice Hockey", image: "icehockey_field_selection"))
        let selectorSize = CGSize(width: 2 * size.width / 3, height: 3 * size.height / 10)
        let selector = ItemSelector(size: selectorSize, itemDistance: 1, items: fields, title: "Select a field", horizontal: true)
        selector.position = CGPoint(x: -size.width / 6, y: size.height / 3)
        addChild(selector)
        return selector
    }
    
    func createTeamSelector(size: CGSize, position: CGPoint, title: String) -> ItemSelector {
        var teams: [SelectableImageItem] = []
        for name in MatchSettingsScene.teamNames {
            teams.append(SelectableImageItem(text: capitalizeFirstLetter(name), image: name))
        }
        let selector = ItemSelector(size: size, itemDistance: 1, items: teams, title: title, horizontal: true)
        selector.position = position
        addChild(selector)
        return selector
    }
    
    func capitalizeFirstLetter(_ string: String) -> String {
        return string.prefix(1).uppercased() + string.lowercased().dropFirst()
    }
    
    func createModeSelector() -> ItemSelector {
        var modes: [SelectableImageItem] = []
        modes.append(SelectableImageItem(value: GameConfig.GameMode.TIME, text: "Time", image: "mode_time"))
        modes.append(SelectableImageItem(value: GameConfig.GameMode.POINTS, text: "Points", image: "mode_points"))
        let selectorSize = CGSize(width: size.width / 2 - 20, height: 3 * size.height / 10)
        let selector = ItemSelector(size: selectorSize, itemDistance: 1, items: modes, title: "Select a mode", horizontal: true)
        selector.position = CGPoint(x: -(size.width / 4 + 10), y: -size.height / 3)
        selector.setOnItemChangeListener(callback: onMenuItemChanged(item:))
        addChild(selector)
        return selector
    }
    
    func onMenuItemChanged(item: ItemSelector.Item) {
        updateModeSettingsSelector()
    }
    
    func createModeSettingsSelector() -> ItemSelector {
        modeTimeItems = createModeTimeItems()
        modePointsItems = createModePointsItems()
        let selectorSize = CGSize(width: size.width / 2, height: 3 * size.height / 10)
        let selector = ItemSelector(size: selectorSize, itemDistance: 1.5, items: modeTimeItems, title: "Game Duration", horizontal: false)
        selector.position = CGPoint(x: size.width / 4, y: -size.height / 3)
        addChild(selector)
        return selector
    }
    
    func createModeTimeItems() -> [ItemSelector.Item] {
        var options: [SelectableLabelItem] = []
        options.append(SelectableLabelItem(value: 30, text: "0:30"))
        options.append(SelectableLabelItem(value: 60, text: "1:00"))
        options.append(SelectableLabelItem(value: 90, text: "1:30"))
        options.append(SelectableLabelItem(value: 120, text: "2:00"))
        options.append(SelectableLabelItem(value: 150, text: "2:30"))
        options.append(SelectableLabelItem(value: 180, text: "3:00"))
        options.append(SelectableLabelItem(value: 300, text: "5:00"))
        options.append(SelectableLabelItem(value: 450, text: "7:30"))
        options.append(SelectableLabelItem(value: 600, text: "10:00"))
        options.append(SelectableLabelItem(value: 5400, text: "90:00"))
        return options
    }
    
    func createModePointsItems() -> [ItemSelector.Item] {
        var options: [SelectableLabelItem] = []
        for i in 1...10 {
            options.append(SelectableLabelItem(value: i, text: "\(i) Goals"))
        }
        return options
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let location = (touches.first?.location(in: self))!
        if btnPlayGame.contains(location) {
            //AVAudioPlayerPool.stop()
            //AVAudioPlayerPool.playButtonClickSound()
            let gameScene = GameScene(size: view!.bounds.size, gameConfig: getGameConfig())
            if(team1Selector.currentItem.value as! String == team2Selector.currentItem.value as! String){
                sameTeamDialog.createSameTeamDialog(scene: self, width: frame.width, height: frame.height)
            }else{
                AVAudioPlayerPool.stop()
                AVAudioPlayerPool.playButtonClickSound()
                view!.presentScene(gameScene)
            }
        }
        if btnBack.contains(location) {
            AVAudioPlayerPool.stop()
            AVAudioPlayerPool.playButtonClickSound()
            let startMenu = StartMenuScene(size: view!.bounds.size)
            startMenu.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            view!.presentScene(startMenu)
        }
        if sameTeamDialog.agreeButton.contains(location){
            sameTeamDialog.removeDialog()
        }

    }
    
    func updateModeSettingsSelector() {
        var title: String
        var items: [ItemSelector.Item]
        if modeSelector.currentItem.value as! GameConfig.GameMode == GameConfig.GameMode.POINTS {
            title = "Score To Win"
            items = modePointsItems
        } else {
            title = "Game Duration"
            items = modeTimeItems
        }
        modeSettingsSelector.titleLabel.text = title
        modeSettingsSelector.currentItem = items[0]
        modeSettingsSelector.currentItemIndex = 0
        modeSettingsSelector.currentItem.highlightItem()
        modeSettingsSelector.replaceAllItems(items: items)
    }
    
    func getGameConfig() -> GameConfig {
        let gameConfig = GameConfig()
        gameConfig.gameField = fieldSelector.currentItem.value as! GameConfig.GameField
        gameConfig.team1 = team1Selector.currentItem.value as! String
        gameConfig.team2 = team2Selector.currentItem.value as! String
        gameConfig.gameMode = modeSelector.currentItem.value as! GameConfig.GameMode
        gameConfig.gameEndCondition = modeSettingsSelector.currentItem.value as! Int
        return gameConfig
    }
    
    func loadBackgroundMusik(){
        if let url = Bundle.main.url(forResource: "settingmusic_1_0", withExtension: "mp3") {
            let backgroundMusik = AVAudioPlayerPool.player(url: url, sound: false)!
            backgroundMusik.audioPlayer.numberOfLoops = -1
            backgroundMusik.audioPlayer.play()
        }
    }
    
}
