//
//  TeamOnline.swift
//  SmashIT
//
//  Created by student on 17.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import Foundation

class TeamOnline: Team {
    
    private let playerId: String
    
    init(name: String, texture: String, isCurrentlyPlaying: Bool, amount: Int, playerId: String) {
        self.playerId = playerId
        super.init(name: name, texture: texture, isCurrentlyPlaying: isCurrentlyPlaying, amount: amount)
    }
}
