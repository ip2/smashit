//
//  GameTimer.swift
//  SmashIT
//
//  Created by student on 03.05.19.
//  Copyright © 2019 HFT Stuttgart. All rights reserved.
//

import SpriteKit

class GameTimer: SKNode {
    
    var remainingTime = 0
    var timeLabel = SKLabelNode()
    var grayOverlay = SKSpriteNode()
    
    func initializeGameTimer() {
        createGrayOverlay()
        timeLabel = SKLabelNode(text: "\(remainingTime)")
        timeLabel.position = CalcSize.calcNodePosition(position: CGPoint(x: -0.5, y: 0.85))
        timeLabel.fontSize = CalcSize.calcFontsize(fontSize: 60)
        timeLabel.fontName = "AvenirNext-Bold"
        timeLabel.zPosition = 0.2
        addChild(timeLabel)
    }
    
    fileprivate func createGrayOverlay() {
        if remainingTime > 99 {
            grayOverlay = SKSpriteNode(color: .gray, size: CGSize(width: CalcSize.calcRelativeNodeWidth(width: 100), height: CalcSize.calcRelativeNodeWidth(width: 65)))
        } else {
            grayOverlay = SKSpriteNode(color: .gray, size: CalcSize.calcRelativeNodeSizeForSqare(size: 65))
        }
        grayOverlay.position = CalcSize.calcNodePosition(position: CGPoint(x: -0.5, y: 0.9))
        grayOverlay.zPosition = 0.1
        grayOverlay.alpha = 0.7
        addChild(grayOverlay)
    }
    
    func startGameTimer(funcToCall: @escaping () -> ()){
        let timeInterval = SKAction.wait(forDuration: 1)
        let updateTimer = SKAction.run({
            if self.remainingTime > 0 {
                self.remainingTime -= 1
                self.timeLabel.text = "\(self.remainingTime)"
                if self.remainingTime <= 100 {
                    self.grayOverlay.size = CalcSize.calcRelativeNodeSizeForSqare(size: 65)
                }
            } else {
                self.removeAction(forKey: "countdown")
                funcToCall()
            }
        })
        let sequence = SKAction.sequence([timeInterval,updateTimer])
        run(SKAction.repeatForever(sequence), withKey: "countdown")
    }
}
